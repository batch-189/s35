const express = require('express')
const dotenv = require('dotenv')
const mongoose = require('mongoose')

const app = express()
const port = 4000

dotenv.config()

mongoose.connect(`mongodb+srv://admin12345:${process.env.MONGODB_PASSWORD}@zuitt-bootcamp.qquvkqu.mongodb.net/?retryWrites=true&w=majority`, {

		useNewUrlParser : true,
		useUnifiedTopology: true
})

const db = mongoose.connection

db.on('error', console.error.bind(console, 'Connection Error'))
db.on('open', () => console.log('Connected to MongoDB!'))

const userSchema = new mongoose.Schema({
	username: String,
	password: String
	
})
const User =  mongoose.model('User',userSchema)

app.post('/signup', (request,response) => {
		User.findOne({username: request.body.username}, (error,result) => {
			if(result != null && result.username == request.body.username){
					return response.send('Your username is already exist')
			} else{
				if(request.body.username !== '' && request.body.password !== '' ){
					let newUser = new User({
						username: request.body.username,
						password: request.body.password
					})
					newUser.save((error,savedUser) => {
						if(error){
							return response.error(error)
						}
							return response.send('New user registered')
					})
				}
				else{
					return response.send(`Please provide username and password`)
				}
			}
		})

})